# Start using Git on the command line

# How to create your SSH keys

This topic describes how to create SSH keys. You do this to use Git over SSH instead of Git over HTTP.

## Creating your SSH keys

1. Go to your [command line](start-using-git.md) and follow the [instructions](ssh.md) to generate your SSH key pair.
1. Log in to GitLab.
1. In the upper-right corner, click your avatar and select **Settings**.
1. On the **User Settings** menu, select **SSH keys**.
1. Paste the **public** key generated in the first step in the **Key**
   text field.
1. Optionally, give it a descriptive title so that you can recognize it in the
   event you add multiple keys.
1. Finally, click the **Add key** button to add it to GitLab. You will be able to see
   its fingerprint, title, and creation date.

    ![SSH key single page](img/profile_settings_ssh_keys_single_key.png)

NOTE: **Note:**
Once you add a key, you cannot edit it. If the paste
didn't work, you need  to remove the offending key and re-add it.


## Basic Git commands

Start using Git via the command line with the most basic
commands as described below.

### Clone a repository

To start working locally on an existing remote repository,
clone it with the command `git clone <repository path>`.
By cloning a repository, you'll download a copy of its
files into your local computer, preserving the Git
connection with the remote repository.

You can either clone it via HTTPS or [SSH](../ssh/README.md).
If you chose to clone it via HTTPS, you'll have to enter your
credentials every time you pull and push. With SSH, you enter
your credentials once and can pull and push straightaway.

You can find both paths (HTTPS and SSH) by navigating to
your project's landing page and clicking **Clone**. GitLab
will prompt you with both paths, from which you can copy
and paste in your command line.

As an example, consider a repository path:

- HTTPS: `https://gitlab.com/nukespud/intro_to_gitlab.git`
- SSH: `` git@gitlab.com:nukespud/intro_to_gitlab.git ``

To get started, open a terminal window in the directory
you wish to clone the repository files into, and run one
of the following commands.

Clone via HTTPS:

```bash
git clone https://gitlab.com/nukespud/intro_to_gitlab.git
```

Clone via SSH:

```bash
git clone git@gitlab.com:nukespud/intro_to_gitlab.git
```

Both commands will download a copy of the files in a
folder named after the project's name.

You can then navigate to the directory and start working
on it locally.

### Go to the master branch to pull the latest changes from there

```bash
git checkout master
```

### Download the latest changes in the project

This is for you to work on an up-to-date copy (it is important to do this every time you start working on a project), while you set up tracking branches. You pull from remote repositories to get all the changes made by users since the last time you cloned or pulled the project. Later, you can push your local commits to the remote repositories.

```bash
git pull REMOTE NAME-OF-BRANCH
```

When you first clone a repository, REMOTE is typically "origin". This is where the repository came from, and it indicates the SSH or HTTPS URL of the repository on the remote server. NAME-OF-BRANCH is usually "master", but it may be any existing branch.

### View your remote repositories

To view your remote repositories, type:

```bash
git remote -v
```

### Create a branch

To create a branch, type the following (spaces won't be recognized in the branch name, so you will need to use a hyphen or underscore):

```bash
git checkout -b NAME-OF-BRANCH
```

### Work on an existing branch

To switch to an existing branch, so you can work on it:

```bash
git checkout NAME-OF-BRANCH
```

### View the changes you've made

It's important to be aware of what's happening and the status of your changes. When you add, change, or delete files/folders, Git knows about it. To check the status of your changes:

```bash
git status
```

### View differences

To view the differences between your local, unstaged changes and the repository versions that you cloned or pulled, type:

```bash
git diff
```

### Add and commit local changes

You'll see your local changes in red when you type `git status`. These changes may be new, modified, or deleted files/folders. Use `git add` to stage a local file/folder for committing. Then use `git commit` to commit the staged files:

```bash
git add FILE OR FOLDER
git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"
```

### Add all changes to commit

To add and commit all local changes in one command:

```bash
git add .
git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"
```

NOTE: **Note:**
The `.` character typically means _all_ in Git.

### Send changes to gitlab.com

To push all local commits to the remote repository:

```bash
git push REMOTE NAME-OF-BRANCH
```

For example, to push your local commits to the _master_ branch of the _origin_ remote:

```bash
git push origin master
```

### Delete all changes in the Git repository

To delete all local changes in the repository that have not been added to the staging area, and leave unstaged files/folders, type:

```bash
git checkout .
```

### Delete all untracked changes in the Git repository

```bash
git clean -f
```

### Unstage all changes that have been added to the staging area

To undo the most recent add, but not committed, files/folders:

```bash
git reset .
```

### Undo most recent commit

To undo the most recent commit, type:

```bash
git reset HEAD~1
```

This leaves the files and folders unstaged in your local repository.

CAUTION: **Warning:**
A Git commit is mostly irreversible, particularly if you already pushed it to the remote repository. Although you can undo a commit, the best option is to avoid the situation altogether.

### Merge created branch with master branch

You need to be in the created branch.

```bash
git checkout NAME-OF-BRANCH
git merge master
```

### Merge master branch with created branch

You need to be in the master branch.

```bash
git checkout master
git merge NAME-OF-BRANCH
```
