# Command Line basic commands

NOTE:  If you do not have git installed on your computer command line you can use the website https://cloud.google.com/shell/ and click "Launch Cloud Shell":



#If you want to start using Git and GitLab, make sure that you have created and/or signed into an account on GitLab.

## Open a shell

Depending on your operating system, you will need to use a shell of your preference. Here are some suggestions:

- [Terminal](http://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line) on  Mac OSX

- [GitBash](https://msysgit.github.io) on Windows

- [Linux Terminal](http://www.howtogeek.com/140679/beginner-geek-how-to-start-using-the-linux-terminal/) on Linux

## Check if Git has already been installed

Git is usually preinstalled on Mac and Linux.

Type the following command and then press enter:

```bash
git --version
```

You should receive a message that tells you which Git version you have on your computer. If you don’t receive a "Git version" message, it means that you need to [download Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

If Git doesn't automatically download, there's an option on the website to [download manually](https://git-scm.com/downloads). Then follow the steps on the installation window.

After you are finished installing Git, open a new shell and type `git --version` again to verify that it was correctly installed.

## Add your Git username and set your email

It is important to configure your Git username and email address, since every Git commit will use this information to identify you as the author.

On your shell, type the following command to add your username:

```bash
git config --global user.name "YOUR_USERNAME"
```

Then verify that you have the correct username:

```bash
git config --global user.name
```

To set your email address, type the following command:

```bash
git config --global user.email "your_email_address@example.com"
```

To verify that you entered your email correctly, type:

```bash
git config --global user.email
```

You'll need to do this only once, since you are using the `--global` option. It tells Git to always use this information for anything you do on that system. If you want to override this with a different username or email address for specific projects, you can run the command without the `--global` option when you’re in that project.

## Check your information

To view the information that you entered, along with other global options, type:

```bash
git config --global --list
```

## Start working on your project

In Git, when you copy a project you say you "clone" it. To work on a git project locally (from your own computer), you will need to clone it. To do this, sign in to GitLab.

When you are on your Dashboard, click on the project that you'd like to clone.
To work in the project, you can copy a link to the Git repository through a SSH
or a HTTPS protocol. SSH is easier to use after it's been
[set up](create-your-ssh-keys.md). While you are at the **Project** tab, select
HTTPS or SSH from the dropdown menu and copy the link using the _Copy URL to clipboard_
button (you'll have to paste it on your shell in the next step).

![Copy the HTTPS or SSH](img/project_clone_url.png)

## On the command line

This section has examples of some basic shell commands that you might find useful. For more information, search the web for _bash commands_.

### Clone your project

Go to your computer's shell and type the following command with your SSH or HTTPS URL:

```
git clone PASTE HTTPS OR SSH HERE
```

A clone of the project will be created in your computer.

>**Note:** If you clone your project via a URL that contains special characters, make sure that characters are URL-encoded.

### Go into a project directory to work in it

```
cd NAME-OF-PROJECT
```

### Go back one directory

```
cd ..
```

### List what’s in the current directory

```
ls
```

### List what’s in the current directory that starts with `a`

```
ls a*
```

### List what’s in the current directory that ends with `.md`

```
ls *.md
```

### Create a new directory

```
mkdir NAME-OF-YOUR-DIRECTORY
```

### Create a README.md file in the current directory

```
touch README.md
nano README.md
#### ADD YOUR INFORMATION
#### Press: control + X
#### Type: Y
#### Press: enter
```

### Show the contents of the README.md file

```
cat README.md
```

### Remove a file

```
rm NAME-OF-FILE
```

### Remove a directory and all of its contents

```
rm -r NAME-OF-DIRECTORY
```

### View command history

```
history
```

### Execute command 123 from history

```
!123
```

### Carry out commands for which the account you are using lacks authority

You will be asked for an administrator’s password.

```
sudo
```

### Show which directory I am in

```
pwd
```

### Clear the shell window

```
clear
```
### Sample Git taskflow

If you are completely new to Git, looking through some [sample taskflows](https://rogerdudler.github.io/git-guide/) will help you understand best practices for using these commands as you work.
